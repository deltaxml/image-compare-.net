@echo off
..\..\bin\deltaxml-docbook.exe compare  ^
                articleA/docbook-article-A-db5.xml  articleB/docbook-article-B-db5.xml docbook-article-revision.xml ^
                indent=yes image-compare=true show-image-changes-at-mediaobject-level=true
                