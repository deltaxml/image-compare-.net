# Image Compare sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DocBook-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the  files.*

*For example `DeltaXML-DocBook-Compare-5_2_2_n/samples/sample-name`.*

---

## Running the sample from the Command line

It is possible to compare two DocBook files using the command line tool from sample directory as follows.

    ../../bin/deltaxml-docbook.exe compare articleA/docbook-article-A-db5.xml articleB/docbook-article-B-db5.xml docbook-article-revision.xml indent=yes image-compare=true show-image-changes-at-mediaobject-level=true
    
The docbook-article-revision.xml file contains the result, which shows cases where images have changed as image-compare is set to true. In order to make it more obvious what has changed show-image-changes-at-mediaobject-level has also been set to true. 

Other parameters are available as discussed in the [User Guide](https://docs.deltaxml.com/docbook-compare/latest/user-guide) and summarised by the following command

    ../../bin/deltaxml-docbook.exe describe
   